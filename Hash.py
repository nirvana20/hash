LETTERS = 'acdegilmnoprstuw'

def compute_hash(string):

    	h = 7
    	for char in string:
        	h = h * 37 + LETTERS.index(char)
	return h

def reverse_hash(num):
	s = ''
	t = num
	while(num > 7):
		try:
			s = LETTERS[num%37] + s
			num = (num - num%37)/37
		except:
			print "No Valid String for this number"
			return 
#	print s
	return s

if __name__ == "__main__":
	print "string for number : 930846109532517 is"
	print reverse_hash(930846109532517)
	raw = raw_input("Enter a number to decode : ")
	print reverse_hash(int(raw))

